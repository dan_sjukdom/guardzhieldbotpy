# GuardBot

This is a bot used to report incidencies and send your location so it's tagged as a _dangerous_ place and will show up on a map in the front end application.


Incidencies:

* Accident
* Robbery
* ...


Powered by:

[RASA]()
[Spacy]()
[Flask]()



## Conversation examples

```yaml
person:
	Hola
bot:
	Hola, soy el GuardBot. Puedes reportar incidencias conmigo respecto a lo siguiente:
	* Accidente
	* Asalto
	* Robo
	Podrias indicarme lo que te gustaria reportar? (Quiero reportar un [tipo de incidente])

person:
	Quiero reportar un asalto
bot:
	Podrias darme la descripccion del asalto

person:
	Vi que a una persona la asaltaron cuando bajaba de su coche y se llevaron su automovil.
bot: 
	Me puedes indicar la direccion donde ocurrio el asalto

person:
	Calle xyz ...
bot: 
	Agregare tu reporte a la base de datos, ahora mismo contactare con un agente de seguridad. 
	Te gustaria agregar una foto del lugar donde ocurrio?
	1) Si
	2) No

person:
	1
bot: 
	Puedes enviar una o mas fotos del lugar donde ocurrio

person:
	imagen1, imagen2, ...
bot: 
	Gracias port tu reporte.
```



## Rasa development

```bash
$ pip install rasa[spacy]
$ pip -m spacy download es_core_news_md
$ pip -m spacy link es_core_news_md es
```



* Actions:
    * enviar_incidente:
        * Enviar el incidente reportado por el usuario a la base de datos en la tabla __incidentes__.
    
    * consultar_incidentes:
        * Consultar la base de datos para traer una lista de incidentes reportados, parametros:
            * coordenadas (longitud, latitud)
            * radio (opcional)
    
    


## RASA NLU: Understand input sentences

Extracts:

* Intends: what the user wants to express
    * buy
    * report
    * greet
    * action or verb

* Entities: NER (Named Entity Recognition)
    * person
    * place
    
From the raw and non-structured user messages.


## RASA CORE: The brain

Decides how to reply the conversation and how to keep the flow going using the extracted features _intends_ and _entities_.

Also, it can learn from new conversations since the assistant can be shared along with the preexisting conversations.


## Training the bot

### nlu.yml

Define the entities and also the intends.

### rules.yml

Hard coded rules for a conversation flow (how the bot must always reply for a given input).

### stories.yml

Model the flow of a conversation.


### actions.py

Define custom actions the bot must perform like:
    * querying a database
    * making a call to an API
    * scrape something



## Processing Pipeline

A pipeline defines the way the raw text will be processed in order to extract intends and entities. There are built in
pipelines but you can create custom ones.

* Embeddings (Maps a word to a vector of N features into a vectorial space where similar words are clustered)
    * Pretrained: 
        * Spacy: 
            * There are words that might not be contemplated (very specific words, like domain knowledge)
            * Use it if you don't have a lot of training data and if your vocabulary isn't complex
    * Supervised:
        * Can extract multiple intends from a given sentence
    
