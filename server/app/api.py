from flask_restful import Api
from app import app
from app.resources import (
    IndexAPI
)


api = Api(app)


api.add_resource(IndexAPI, "/api/")
