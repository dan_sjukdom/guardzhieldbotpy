# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []


class ActionAgregarIncidenteDB(Action):
    """
        Agregar el incidente a la base de datos.
        Tabla:
            - incidentes
        Campos:
            - tipo_incidente
            - ubicacion
    """

    def name(self) -> Text:
        return "action_agregar_incidente_db"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        entities = tracker.latest_message["entities"]

        print("*"*10)
        print(dir(entities))
        print(entities)

        dispatcher.utter_message(text="Agregando el incidente reportado a la base de datos")

        return []


class ActionConsultarIncidenteDireccion(Action):
    """
        Consultar los incidentes usando la direccion especificada.
        - Direccion:
            Transformar la direccion a coordenadas.
            Las coordenadas son las que se usan para la query de la base de datos.
        - Entidades:
            Las entidades que representan a un incidente son:
                - incidentes (general)
                - asaltos
                - robos
    """
    def name(self) -> Text:
        return "action_consultar_incidente_direccion"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        entities = tracker.latest_message["entities"][0]
        print(entities)
        direccion = entities.get("value")

        dispatcher.utter_message(text=f"Consultando los incidentes en {direccion}")

        return []


class ActionConsultarIncidenteMiUbicacion(Action):
    """
        Consultar los incidentes usando la ubicacion que provee el GPS del
        dispositivo.

        - Entidades:
            Las entidades que representan a un incidente son:
                - incidentes (general)
                - asaltos
                - robos
    """
    def name(self) -> Text:
        return "action_consultar_incidente_ubicacion"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Consultando los incidentes en tu ubicacion")

        return []

"""
version: "2.0"

intents:
  - saludar
  - despedir
  - afirmar
  - negar
  - ayudar
  - reportar_incidente
  - consultar_incidente_direccion
  - consultar_incidente_mi_ubicacion
  - dar_descripccion_incidente

entities:
  - direccion
  - tipo_incidente
  - tipo_accidente
  - objeto_accidente

actions:
  - action_consultar_incidente_direccion
  - action_consultar_incidente_mi_ubicacion
  - action_agregar_incidente_db

responses:
  utter_greet:
    - text: "Hey! How are you?"

  utter_saludar:
    - text: "Hola, soy GuardBot"
#      Hola, soy GuardBot!.
#
#      Me puedes reportar incidentes del siguiente tipo
#      * Accidentes
#      * Asaltos
#      * Robos
#
#      Para indicarlo, puedes decirlo de la siguiente forma: Quiero reportar un [tipo de incidente aqui]
#
#      Tambien puedes consultar los incidentes que han ocurrido en un lugar en particular.
#
#      Para indicarlo, puedes decirlo de la siguiente forma: Quiero consultar los incidentes de este lugar
#
#      Si requieres de ayuda para saber como interactuar conmigo de forma completa puedes escribir: ayuda"

  utter_ayudar:
    - text: "No ayudar a humano!"
#      Para reportar incidentes me lo puedes indicar escribiendo
#      Quiero reportar un [tipo de incidente aqui]
#
#      Para consultar incidentes puedes escribir la direccion de tu interes o darme tu ubicacion.
#
#      Ejemplos:
#        - Incidentes en general:
#          - Quiero consultar los incidentes en [insertar direccion aqui]
#          - Quiero consultar los incidentes en este lugar
#        - Incidentes en especifico:
#            - Quiero consultar los incidentes"


  utter_despedir:
    - text: "Hasta luego! Espero haberte ayudado."

  utter_afirmar:
    - text:

  utter_negar:
    - text: "Entonces chinga a tu madre"

  utter_reportar_incidente:
    - text: "Me puedes dar la descripccion del incidente"
#    ¿Me podrías describir el incidente describiendo el tipo del mismo y como ocurrió?
#    Ejemplo: Vi un [tipo de incidente] [descripccion incidente]"

  utter_ofrecer_servicio:
    - text: "¿Te gustaria reportar algún otro incidente?"


session_config:
  session_expiration_time: 60
  carry_over_slots_to_new_session: true
"""